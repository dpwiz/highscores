module Highscores where

import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO(..))
import Crypto.Hash.MD5 (hash)
import Data.Aeson (Value, eitherDecode', FromJSON)
import Data.Function ((&))
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.UUID (UUID)
import GHC.Generics (Generic)
import qualified Data.ByteString.Base16 as Base16
import qualified Data.ByteString.Char8 as BS8
import qualified Data.UUID as UUID
import qualified Network.HTTP.Client as HTTP

data App = App
  { appId     :: UUID
  , appSecret :: UUID
  }

baseUrl :: [Char]
baseUrl = "https://www.highscores.ovh/api/highscore"

data Submit = Submit
  { submitPlayerId   :: UUID
  , submitPlayerName :: Text
  , submitScore      :: Integer
  , submitOrder      :: Order
  }

type SubmitResponse = Value

submit
  :: MonadIO m
  => App
  -> HTTP.Manager
  -> Submit
  -> m ()
submit App{..} manager Submit{..} = liftIO $ do
  requestBase <- HTTP.parseUrlThrow ("POST " <> baseUrl)
  let
    request = requestBase &
      HTTP.setQueryString (map (fmap Just) query)

  void $! HTTP.httpNoBody request manager

  where
    query = mappend params
      [ ("checksum", checksum)
      ]

    params =
      [ ("appId",      UUID.toASCIIBytes appId)
      , ("playerName", encodeUtf8 submitPlayerName)
      , ("playerId",   playerId)
      , ("score",      score)
      , ("order",      orderBS submitOrder)
      ]

    playerId = UUID.toASCIIBytes submitPlayerId
    score = BS8.pack $ show submitScore

    checksum =
      Base16.encode . hash $ mconcat
        [ playerId
        , score
        , UUID.toASCIIBytes appSecret
        ]

data Order
  = Desc -- ^ Higher is better
  | Asc -- ^ Lower is better
  deriving (Eq, Ord, Show, Enum, Bounded)

fetch
  :: MonadIO m
  => App
  -> HTTP.Manager
  -> UUID
  -> Order
  -> m (Either String ScoreBoard)
fetch App{appId} manager playerId fetchOrder = liftIO $ do
  requestBase <- HTTP.parseUrlThrow ("GET " <> baseUrl)
  let
    request = requestBase &
      HTTP.setQueryString (map (fmap Just) query)

  response <- HTTP.httpLbs request manager
  pure . eitherDecode' $ HTTP.responseBody response
  where
    query =
      [ ("appId",    UUID.toASCIIBytes appId)
      , ("playerId", UUID.toASCIIBytes playerId)
      , ("order",    orderBS fetchOrder)
      ]

orderBS :: Order -> BS8.ByteString
orderBS order = case order of
  Asc  -> "ASC"
  Desc -> "DESC"

data ScoreBoard = ScoreBoard
  { currentPlayerScore :: Maybe Score
  , topScores          :: [Score]
  }
  deriving (Eq, Show, Generic)

instance FromJSON ScoreBoard

data Score = Score
  { score      :: Int
  , playerName :: Text
  , playerId   :: UUID
  , placement  :: Int
  , scoreTime  :: Text
  }
  deriving (Eq, Show, Generic)

instance FromJSON Score
