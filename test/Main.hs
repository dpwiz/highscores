module Main where

import Data.Foldable (traverse_)
import Data.UUID (UUID)
import GHC.Clock (getMonotonicTimeNSec)
import Network.HTTP.Client.TLS (newTlsManager)

import qualified Highscores

main :: IO ()
main = do
  manager <- newTlsManager

  start <- getMonotonicTimeNSec
  top <- Highscores.fetch app manager testPlayerId Highscores.Asc
  case top of
    Left err ->
      fail $ "Scoreboard parse failed: " <> err
    Right Highscores.ScoreBoard{..} -> do
      putStrLn "Current player score:"
      print currentPlayerScore
      putStrLn ""
      putStrLn "Top scores:"
      traverse_ print topScores
  stop <- getMonotonicTimeNSec

  Highscores.submit app manager Highscores.Submit
    { submitPlayerId   = testPlayerId
    , submitPlayerName = "test"
    , submitScore      = fromIntegral $ (stop - start) `div` 1000
    , submitOrder      = Highscores.Desc
    }

testPlayerId :: UUID
testPlayerId = read "9fda4653-c654-4b35-8c5c-08e8358e630f"

app :: Highscores.App
app = Highscores.App
  { appId     = read "3f3e7479-2e72-4b18-a0bc-a244c8b72a53"
  , appSecret = read "356816fc-ad3e-4e95-a5ca-f80de93c2634"
  }
