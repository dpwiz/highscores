# highscores

A haskell client library for...

## Simple Highscore Service

- It allows to implement simple online highscore system into your game with miminum effort
- It's meant to be used for prototyping and game jams*
- It's totally free
- No registration required

\* This service is not production-ready. Use it at your own risk. Any stored data may be deleted at any time without any notice.

https://www.highscores.ovh/
