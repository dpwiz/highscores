# Changelog for highscores

## 0.3.0.0

- Changed `FetchOrder` to just `Order`.
- Added `Order` to submit too.

## 0.2.0.0

- Added `FetchOrder` argument.

## 0.1.0.0

Initial import.
